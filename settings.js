module.exports = {
  internal: {
    trackchanges: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
